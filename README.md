## Android NumberProgressBar [![Build Status](https://travis-ci.org/daimajia/NumberProgressBar.png?branch=master)](https://travis-ci.org/daimajia/NumberProgressBar)

-----

The NumberProgressBar is a bar, slim and sexy (every man wants! ). 

I decided to do this because I was really tired of android original progress bar. So, I made some change, added more color style for this.

And also you can contribute more color style, or new idea to me.

### Demo

![NumberProgressBar](number_progress.gif)


[Download Demo](https://gitlab.com/joielechong/NumberProgressBar/releases/download/v1.0/NumberProgressBar-Demo-v1.0.apk)


### Usage
----

#### Gradle

**Step 1.** Add the JitPack repository to your build file

Add it in your root build.gradle at the end of repositories:
```groovy
allprojects {
       repositories {
           ...
           maven { url 'https://jitpack.io' }
       }
}
```

**Step 2.** Add the dependency

```groovy
dependencies {
        implementation 'com.gitlab.joielechong:numberprogressbar:1.4.4@aar'
}
```

**Step 3.** Use it in your own code:

```java
<com.rilixtech.numberprogressbar.NumberProgressBar
        android:id="@+id/number_progress_bar"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
/>
```	

I made some pre-design style. You can use them via `style` property.


![Preset color](preset_color.jpg)

Use the preset style just like below:

```java
<com.rilixtech.numberprogressbar.NumberProgressBar
       android:id="@+id/number_progress_bar"
       style="@style/NumberProgressBar_Default"
      />
```	

In the above picture, the style is : 

`NumberProgressBar_Default`
`NumberProgressBar_Passing_Green`
`NumberProgressBar_Relax_Blue`
`NumberProgressBar_Grace_Yellow`
`NumberProgressBar_Warning_Red`
`NumberProgressBar_Funny_Orange`
`NumberProgressBar_Beauty_Red`
`NumberProgressBar_Twinkle_Night`

You can get more beautiful color from [kular](https://kuler.adobe.com), and you can also contribute your color style to NumberProgressBar!  

### Build

run `./gradlew assembleDebug` (Mac/Linux)

or

run `gradlew.bat assembleDebug` (Windows)

### Attributes

There are several attributes you can set:

![](progress_bar_attribute.jpg)

The **reached area** and **unreached area**:

* color
* height 

The **text area**:

* color
* text size
* visibility
* distance between **reached area** and **unreached area**
* text prefix
* text suffix
* text typeface from assets

The **bar**:

* max progress
* current progress
* scale progress value
  Scaling value of progress. For example, to display value as 10 for 100% progress,
  we set the scale with 10.
* progress direction; rtl or ltr

for example, the default style:

```java
<com.rilixtech.numberprogressbar.NumberProgressBar
          android:layout_width="wrap_content"
          android:layout_height="wrap_content"
	        
          custom:npb_unreached_color="#CCCCCC"
          custom:npb_reached_color="#3498DB"
	        
          custom:npb_unreached_bar_height="0.75dp"
          custom:npb_reached_bar_height="1.5dp"
	        
          custom:npb_text_size="10sp"
          custom:npb_text_color="#3498DB"
          custom:npb_text_offset="1dp"
          custom:npb_text_visibility="visible"
	        
          custom:npb_max="100"
          custom:npb_current="80"
          custom:npb_progress_direction="rtl"
          />
```

-----------------

Thanks for [daimaja](https://github.com/daimajia) for the original project at:
https://github.com/daimajia/NumberProgressBar