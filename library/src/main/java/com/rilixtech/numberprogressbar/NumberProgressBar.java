package com.rilixtech.numberprogressbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by daimajia on 14-4-30.
 * Updated by Joielechong on 6 September 2018
 */
public class NumberProgressBar extends View {

  private int mMaxProgress = 100;

  private static final int PROGRESS_LTR = 0;
  private static final int PROGRESS_RTL = 1;

  /**
   * Current progress, can not exceed the max progress.
   */
  private int mCurrentProgress = 0;

  /**
   * Scale of current progress.
   */
  private int mScaleProgress = 0;

  /**
   * The progress area bar color.
   */
  private int mReachedBarColor;

  /**
   * The bar unreached area color.
   */
  private int mUnreachedBarColor;

  /**
   * The progress text color.
   */
  private int mTextColor;

  /**
   * The progress text size.
   */
  private float mTextSize;

  /**
   * The height of the reached area.
   */
  private float mReachedBarHeight;

  /**
   * The height of the unreached area.
   */
  private float mUnreachedBarHeight;

  /**
   * The suffix of the number.
   */
  private String mSuffix;

  /**
   * The prefix.
   */
  private String mPrefix;

  private static final int DEFAULT_TEXT_COLOR = Color.rgb(66, 145, 241);
  private static final int DEFAULT_REACHED_COLOR = Color.rgb(66, 145, 241);
  private static final int DEFAULT_UNREACHED_COLOR = Color.rgb(204, 204, 204);
  public static final String DEFAULT_SUFFIX = "%";

  /**
   * For save and restore instance of progressbar.
   */
  private static final String INSTANCE_STATE = "saved_instance";
  private static final String INSTANCE_TEXT_COLOR = "text_color";
  private static final String INSTANCE_TEXT_SIZE = "text_size";
  private static final String INSTANCE_REACHED_BAR_HEIGHT = "reached_bar_height";
  private static final String INSTANCE_REACHED_BAR_COLOR = "reached_bar_color";
  private static final String INSTANCE_UNREACHED_BAR_HEIGHT = "unreached_bar_height";
  private static final String INSTANCE_UNREACHED_BAR_COLOR = "unreached_bar_color";
  private static final String INSTANCE_MAX = "max";
  private static final String INSTANCE_PROGRESS = "progress";
  private static final String INSTANCE_SUFFIX = "suffix";
  private static final String INSTANCE_PREFIX = "prefix";
  private static final String INSTANCE_TEXT_VISIBILITY = "text_visibility";
  private static final String INSTANCE_SCALE = "scale";
  private static final String INSTANCE_TYPE_FACE = "typeface";
  private static final String INSTANCE_PROGRESS_DIRECTION = "direction";

  private static final int PROGRESS_TEXT_VISIBLE = 0;

  /**
   * The width of the text that to be drawn.
   */
  private float mDrawTextWidth;

  /**
   * The drawn text start.
   */
  private float mDrawTextStart;

  /**
   * The drawn text end.
   */
  private float mDrawTextEnd;

  /**
   * The text that to be drawn in onDraw().
   */
  private String mCurrentDrawText;

  /**
   * The Paint of the reached area.
   */
  private Paint mReachedBarPaint;
  /**
   * The Paint of the unreached area.
   */
  private Paint mUnreachedBarPaint;
  /**
   * The Paint of the progress text.
   */
  private Paint mTextPaint;

  /**
   * Unreached bar area to draw rect.
   */
  private RectF mUnreachedRectF = new RectF(0, 0, 0, 0);
  /**
   * Reached bar area rect.
   */
  private RectF mReachedRectF = new RectF(0, 0, 0, 0);

  /**
   * The progress text offset.
   */
  private float mOffset;

  /**
   * Determine if need to draw unreached area.
   */
  private boolean mDrawUnreachedBar = true;

  private boolean mDrawReachedBar = true;

  private boolean mIfDrawText = true;

  private String mTextTypefacePath;
  private Typeface mTextTypeface;

  private int mDirectionType;

  /**
   * Listener
   */
  private OnProgressBarListener mListener;

  public NumberProgressBar(Context context) {
    this(context, null);
  }

  public NumberProgressBar(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public NumberProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    // set default view size
    final float defaultReachedBarHeight = Util.dp2px(context, 1.5f);
    final float defaultUnreachedBarHeight = Util.dp2px(context, 1.0f);
    final float defaultTextSize = Util.sp2px(context, 10);
    final float defaultProgressTextOffset = Util.dp2px(context, 3.0f);

    //load styled attributes.
    final TypedArray arr = context.getTheme()
        .obtainStyledAttributes(attrs, R.styleable.NumberProgressBar, defStyleAttr, 0);

    mReachedBarColor = arr.getColor(R.styleable.NumberProgressBar_npb_reached_color, DEFAULT_REACHED_COLOR);
    mUnreachedBarColor = arr.getColor(R.styleable.NumberProgressBar_npb_unreached_color, DEFAULT_UNREACHED_COLOR);
    mTextColor = arr.getColor(R.styleable.NumberProgressBar_npb_text_color, DEFAULT_TEXT_COLOR);
    mTextSize = arr.getDimension(R.styleable.NumberProgressBar_npb_text_size, defaultTextSize);
    mSuffix = arr.getString(R.styleable.NumberProgressBar_npb_text_suffix);
    mSuffix = mSuffix == null? DEFAULT_SUFFIX: mSuffix;
    mPrefix = arr.getString(R.styleable.NumberProgressBar_npb_text_prefix);
    mPrefix = mPrefix == null? "": mPrefix;
    mTextTypefacePath = arr.getString(R.styleable.NumberProgressBar_npb_typeface_assets_path);

    mReachedBarHeight = arr.getDimension(R.styleable.NumberProgressBar_npb_reached_bar_height,
        defaultReachedBarHeight);
    mUnreachedBarHeight = arr.getDimension(R.styleable.NumberProgressBar_npb_unreached_bar_height,
        defaultUnreachedBarHeight);
    mOffset = arr.getDimension(R.styleable.NumberProgressBar_npb_text_offset, defaultProgressTextOffset);

    int textVisible = arr.getInt(R.styleable.NumberProgressBar_npb_text_visibility, PROGRESS_TEXT_VISIBLE);
    mIfDrawText = textVisible == PROGRESS_TEXT_VISIBLE;

    mDirectionType = arr.getInt(R.styleable.NumberProgressBar_npb_progress_direction, PROGRESS_LTR);

    mScaleProgress = arr.getInt(R.styleable.NumberProgressBar_npb_scale, 0);

    int maxProgress = arr.getInt(R.styleable.NumberProgressBar_npb_max, 100);
    if (maxProgress > 0) mMaxProgress = maxProgress;

    int progress = arr.getInt(R.styleable.NumberProgressBar_npb_current, 0);
    if (progress <= mMaxProgress && progress >= 0) mCurrentProgress = progress;

    arr.recycle();
    initializePainters();
  }

  @Override protected int getSuggestedMinimumWidth() {
    return (int) mTextSize;
  }

  @Override protected int getSuggestedMinimumHeight() {
    return Math.max((int) mTextSize, Math.max((int) mReachedBarHeight, (int) mUnreachedBarHeight));
  }

  @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    setMeasuredDimension(measure(widthMeasureSpec, true), measure(heightMeasureSpec, false));
  }

  private int measure(int measureSpec, boolean isWidth) {
    int result;
    int mode = MeasureSpec.getMode(measureSpec);
    int size = MeasureSpec.getSize(measureSpec);
    int padding =
        isWidth ? getPaddingLeft() + getPaddingRight() : getPaddingTop() + getPaddingBottom();
    if (mode == MeasureSpec.EXACTLY) {
      result = size;
    } else {
      result = isWidth ? getSuggestedMinimumWidth() : getSuggestedMinimumHeight();
      result += padding;
      if (mode == MeasureSpec.AT_MOST) {
        if (isWidth) {
          result = Math.max(result, size);
        } else {
          result = Math.min(result, size);
        }
      }
    }
    return result;
  }

  @Override protected void onDraw(Canvas canvas) {
    if (mIfDrawText) {
      calculateDrawRectF();
    } else {
      calculateDrawRectFWithoutProgressText();
    }

    if (mDrawReachedBar) canvas.drawRect(mReachedRectF, mReachedBarPaint);

    if (mDrawUnreachedBar) canvas.drawRect(mUnreachedRectF, mUnreachedBarPaint);

    if (mIfDrawText) canvas.drawText(mCurrentDrawText, mDrawTextStart, mDrawTextEnd, mTextPaint);
  }

  private void calculateDrawRectF() {
    switch (mDirectionType) {
      case PROGRESS_LTR:
        calculateDrawRectFLtr();
        break;
      case PROGRESS_RTL:
        calculateDrawRectFRtl();
        break;
    }
  }

  private void calculateDrawRectFWithoutProgressText() {
    switch (mDirectionType){
      case PROGRESS_LTR:
        calculateDrawRectFWithoutProgressTextLtr();
        break;
      case PROGRESS_RTL:
        calculateDrawRectFWithoutProgressTextRtl();
        break;
    }
  }

  private void initializePainters() {
    mReachedBarPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    mReachedBarPaint.setColor(mReachedBarColor);

    mUnreachedBarPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    mUnreachedBarPaint.setColor(mUnreachedBarColor);

    mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    mTextPaint.setColor(mTextColor);
    mTextPaint.setTextSize(mTextSize);

    // only call after we have text paint.
    setTypeface(mTextTypefacePath);
  }

  private void calculateDrawRectFWithoutProgressTextLtr() {
    int padLeft = getPaddingLeft();
    int padRight = getPaddingRight();
    int height = getHeight();
    int width = getWidth();
    int max = getMax();
    int progress = getProgress();


    mReachedRectF.left = padLeft;
    mReachedRectF.top = height / 2.0f - mReachedBarHeight / 2.0f;
    mReachedRectF.right = (width - padLeft - padRight) / (max * 1.0f) * progress + padLeft;
    mReachedRectF.bottom = height / 2.0f + mReachedBarHeight / 2.0f;

    mUnreachedRectF.left = mReachedRectF.right;
    mUnreachedRectF.right = width - padRight;
    mUnreachedRectF.top = height / 2.0f + -mUnreachedBarHeight / 2.0f;
    mUnreachedRectF.bottom = height / 2.0f + mUnreachedBarHeight / 2.0f;
  }

  private void calculateDrawRectFWithoutProgressTextRtl() {
    int padLeft = getPaddingLeft();
    int padRight = getPaddingRight();
    int height = getHeight();
    int width = getWidth();
    int max = getMax();
    int progress = 100 - getProgress();

    mReachedRectF.right = width - padRight;
    mReachedRectF.top = height / 2.0f - mReachedBarHeight / 2.0f;
    mReachedRectF.left = (width - padLeft - padRight) / (max * 1.0f) * progress + padLeft;
    mReachedRectF.bottom = height / 2.0f + mReachedBarHeight / 2.0f;

    mUnreachedRectF.left = padLeft;
    mUnreachedRectF.right = mReachedRectF.left;
    mUnreachedRectF.top = height / 2.0f + -mUnreachedBarHeight / 2.0f;
    mUnreachedRectF.bottom = height / 2.0f + mUnreachedBarHeight / 2.0f;
  }

  private void calculateDrawRectFLtr() {
    if(mScaleProgress == 0) {
      mCurrentDrawText = String.format("%d", getProgress() * 100 / getMax());
    } else {
      mCurrentDrawText = String.format("%d", (getProgress() * 100 / getMax()) / mScaleProgress);
    }
    mCurrentDrawText = mPrefix + mCurrentDrawText + mSuffix;
    mDrawTextWidth = mTextPaint.measureText(mCurrentDrawText);

    final int height = getHeight();
    final int width = getWidth();
    final int padLeft = getPaddingLeft();
    final int padRight = getPaddingRight();
    int progress = getProgress();
    int max = getMax();
    if (progress == 0) {
      mDrawReachedBar = false;
      mDrawTextStart = padLeft;
    } else {
      mDrawReachedBar = true;
      mReachedRectF.left = padLeft;
      mReachedRectF.top = height / 2.0f - mReachedBarHeight / 2.0f;
      mReachedRectF.right = (width - padLeft - padRight) / (max * 1.0f) * progress - mOffset + padLeft;
      mReachedRectF.bottom = height / 2.0f + mReachedBarHeight / 2.0f;
      mDrawTextStart = (mReachedRectF.right + mOffset);
    }

    mDrawTextEnd = (int) ((height / 2.0f) - ((mTextPaint.descent() + mTextPaint.ascent()) / 2.0f));

    if ((mDrawTextStart + mDrawTextWidth) >= width - padRight) {
      mDrawTextStart = width - padRight - mDrawTextWidth;
      mReachedRectF.right = mDrawTextStart - mOffset;
    }

    float unreachedBarStart = mDrawTextStart + mDrawTextWidth + mOffset;
    if (unreachedBarStart >= width - padRight) {
      mDrawUnreachedBar = false;
    } else {
      mDrawUnreachedBar = true;
      mUnreachedRectF.left = unreachedBarStart;
      mUnreachedRectF.right = width - padRight;
      mUnreachedRectF.top = height / 2.0f + -mUnreachedBarHeight / 2.0f;
      mUnreachedRectF.bottom = height / 2.0f + mUnreachedBarHeight / 2.0f;
    }
  }

  private void calculateDrawRectFRtl() {
    if(mScaleProgress == 0) {
      mCurrentDrawText = String.format("%d", getProgress() * 100 / getMax());
    } else {
      mCurrentDrawText = String.format("%d", (getProgress() * 100 / getMax()) / mScaleProgress);
    }
    mCurrentDrawText = mPrefix + mCurrentDrawText + mSuffix;
    mDrawTextWidth = mTextPaint.measureText(mCurrentDrawText);

    final int height = getHeight();
    final int width = getWidth();
    final int padLeft = getPaddingLeft();
    final int padRight = getPaddingRight();
    int regress = 100 - getProgress();
    int max = getMax();
    if (regress > 0) {
      mDrawUnreachedBar = true;
      mUnreachedRectF.top = height / 2.0f - mUnreachedBarHeight / 2.0f;
      mUnreachedRectF.bottom = height / 2.0f + mUnreachedBarHeight / 2.0f;

      mUnreachedRectF.left = padLeft;
      mUnreachedRectF.right = (width - padLeft - padRight - mOffset) / (max * 1.0f) * regress - mOffset + padLeft;

      mDrawTextStart = (mUnreachedRectF.right + mOffset);
    } else {
      mDrawUnreachedBar = false;
      mDrawTextStart = padLeft;
      mUnreachedRectF.left = padLeft;
      mUnreachedRectF.right = padLeft;
    }

    mDrawTextEnd = (int) ((height / 2.0f) - ((mTextPaint.descent() + mTextPaint.ascent()) / 2.0f));

    if ((mDrawTextStart + mDrawTextWidth) >= width - padRight) {
      mDrawTextStart = width - padRight - mDrawTextWidth;
      mUnreachedRectF.right = mDrawTextStart - mOffset;
    }

    float reachedBarStart = mDrawTextStart + mDrawTextWidth + mOffset;
    if (reachedBarStart >= width - padRight) {
      mDrawReachedBar = false;
    } else {
      mDrawReachedBar = true;
      mReachedRectF.left = reachedBarStart;
      mReachedRectF.right = width - padRight;
      mReachedRectF.top = height / 2.0f + -mUnreachedBarHeight / 2.0f;
      mReachedRectF.bottom = height / 2.0f + mUnreachedBarHeight / 2.0f;
    }
  }

  /**
   * Get progress text color.
   *
   * @return progress text color.
   */
  public int getTextColor() {
    return mTextColor;
  }

  /**
   * Get progress text size.
   *
   * @return progress text size.
   */
  public float getProgressTextSize() {
    return mTextSize;
  }

  public int getUnreachedBarColor() {
    return mUnreachedBarColor;
  }

  public int getReachedBarColor() {
    return mReachedBarColor;
  }

  public int getProgress() {
    return mCurrentProgress;
  }

  public int getMax() {
    return mMaxProgress;
  }

  public float getReachedBarHeight() {
    return mReachedBarHeight;
  }

  public float getUnreachedBarHeight() {
    return mUnreachedBarHeight;
  }

  public void setProgressTextSize(float textSize) {
    mTextSize = textSize;
    mTextPaint.setTextSize(mTextSize);
    invalidate();
  }

  public void setProgressTextColor(int textColor) {
    mTextColor = textColor;
    mTextPaint.setColor(mTextColor);
    invalidate();
  }

  public void setUnreachedBarColor(int barColor) {
    mUnreachedBarColor = barColor;
    mUnreachedBarPaint.setColor(mUnreachedBarColor);
    invalidate();
  }

  public void setReachedBarColor(int progressColor) {
    mReachedBarColor = progressColor;
    mReachedBarPaint.setColor(mReachedBarColor);
    invalidate();
  }

  public void setReachedBarHeight(float height) {
    mReachedBarHeight = height;
  }

  public void setUnreachedBarHeight(float height) {
    mUnreachedBarHeight = height;
  }

  public void setMax(int maxProgress) {
    if (maxProgress > 0) {
      mMaxProgress = maxProgress;
      invalidate();
    }
  }

  public void setSuffix(String suffix) {
    if (suffix == null) {
      mSuffix = DEFAULT_SUFFIX;
    } else {
      mSuffix = suffix;
    }
  }

  public String getSuffix() {
    return mSuffix;
  }

  public void setPrefix(String prefix) {
    if (prefix == null) {
      mPrefix = "";
    } else {
      mPrefix = prefix;
    }
  }

  public String getPrefix() {
    return mPrefix;
  }

  public void incrementProgressBy(int by) {
    if (by > 0) setProgress(getProgress() + by);
    if (mListener != null) mListener.onProgressChange(getProgress(), getMax());
  }

  public void setProgress(int progress) {
    if (progress <= getMax() && progress >= 0) {
      mCurrentProgress = progress;
      invalidate();
    }
  }

  @Override protected Parcelable onSaveInstanceState() {
    final Bundle bundle = new Bundle();
    bundle.putParcelable(INSTANCE_STATE, super.onSaveInstanceState());
    bundle.putInt(INSTANCE_TEXT_COLOR, getTextColor());
    bundle.putFloat(INSTANCE_TEXT_SIZE, getProgressTextSize());
    bundle.putFloat(INSTANCE_REACHED_BAR_HEIGHT, getReachedBarHeight());
    bundle.putFloat(INSTANCE_UNREACHED_BAR_HEIGHT, getUnreachedBarHeight());
    bundle.putInt(INSTANCE_REACHED_BAR_COLOR, getReachedBarColor());
    bundle.putInt(INSTANCE_UNREACHED_BAR_COLOR, getUnreachedBarColor());
    bundle.putInt(INSTANCE_MAX, getMax());
    bundle.putInt(INSTANCE_PROGRESS, getProgress());
    bundle.putString(INSTANCE_SUFFIX, getSuffix());
    bundle.putString(INSTANCE_PREFIX, getPrefix());
    bundle.putBoolean(INSTANCE_TEXT_VISIBILITY, getTextVisibility());
    bundle.putInt(INSTANCE_SCALE, mScaleProgress);
    bundle.putString(INSTANCE_TYPE_FACE, mTextTypefacePath);
    bundle.putInt(INSTANCE_PROGRESS_DIRECTION, mDirectionType);
    return bundle;
  }

  @Override protected void onRestoreInstanceState(Parcelable state) {
    if (state instanceof Bundle) {
      final Bundle bundle = (Bundle) state;
      mTextColor = bundle.getInt(INSTANCE_TEXT_COLOR);
      mTextSize = bundle.getFloat(INSTANCE_TEXT_SIZE);
      mReachedBarHeight = bundle.getFloat(INSTANCE_REACHED_BAR_HEIGHT);
      mUnreachedBarHeight = bundle.getFloat(INSTANCE_UNREACHED_BAR_HEIGHT);
      mReachedBarColor = bundle.getInt(INSTANCE_REACHED_BAR_COLOR);
      mUnreachedBarColor = bundle.getInt(INSTANCE_UNREACHED_BAR_COLOR);
      mScaleProgress = bundle.getInt(INSTANCE_SCALE);
      mTextTypefacePath = bundle.getString(INSTANCE_TYPE_FACE);
      mDirectionType = bundle.getInt(INSTANCE_PROGRESS_DIRECTION);
      initializePainters();
      setMax(bundle.getInt(INSTANCE_MAX));
      setProgress(bundle.getInt(INSTANCE_PROGRESS));
      setPrefix(bundle.getString(INSTANCE_PREFIX));
      setSuffix(bundle.getString(INSTANCE_SUFFIX));
      setTextVisibility(bundle.getBoolean(INSTANCE_TEXT_VISIBILITY) ? TextVisibility.VISIBLE
              : TextVisibility.INVISIBLE);
      super.onRestoreInstanceState(bundle.getParcelable(INSTANCE_STATE));
      return;
    }
    super.onRestoreInstanceState(state);
  }

  public void setTextVisibility(TextVisibility visibility) {
    mIfDrawText = visibility == TextVisibility.VISIBLE;
    invalidate();
  }

  public boolean getTextVisibility() {
    return mIfDrawText;
  }

  public void setOnProgressBarListener(OnProgressBarListener listener) {
    mListener = listener;
  }

  public int getScale() {
    return mScaleProgress;
  }

  public void setScale(int scaleProgress) {
    mScaleProgress = scaleProgress;
  }

  public Typeface getTypeface() {
    return mTextTypeface;
  }

  public void setTypeface(String typefacePath) {
    mTextTypefacePath = typefacePath;
    if(typefacePath == null || typefacePath.trim().isEmpty())  {
      mTextTypeface = null;
    } else {
      mTextTypeface = Typeface.createFromAsset(getContext().getAssets(), typefacePath);
    }
    mTextPaint.setTypeface(mTextTypeface);
  }

  public int getProgressDirection() {
    return mDirectionType;
  }

  public void setProgressDirection(int directionType) {
    mDirectionType = directionType;
    invalidate();
  }
}
