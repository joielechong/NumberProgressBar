package com.rilixtech.numberprogressbar;

import android.content.Context;

public class Util {

  static float dp2px(Context context, float dp) {
    final float scale = context.getResources().getDisplayMetrics().density;
    return dp * scale + 0.5f;
  }

  static float sp2px(Context context, float sp) {
    final float scale = context.getResources().getDisplayMetrics().scaledDensity;
    return sp * scale;
  }
}
